import 'package:flutter/material.dart';
import 'package:posts/UI/WelcomeScreen.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Posts',
      debugShowCheckedModeBanner: false,
      home: new Scaffold(
        backgroundColor: Color(0xffeeeeee),
        body: new WelcomeScreen(),
      ),
    );
  }
}
