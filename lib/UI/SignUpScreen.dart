import 'package:flutter/material.dart';

class SignUpScreen extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => _SignUpScreenState();
}

class _SignUpScreenState extends State<SignUpScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: new AppBar(
          centerTitle: true,
          title: Text("Sign-Up"),
          backgroundColor: Color(0xFF005482),
        ),
        backgroundColor: Color(0xffeeeeee),
        body: SingleChildScrollView(
          child: Padding(
            padding: const EdgeInsets.only(right: 35, left: 25, top: 20),
            child: new Column(
              mainAxisAlignment: MainAxisAlignment.start,
              children: <Widget>[
                new Theme(
                    data: Theme.of(context)
                        .copyWith(primaryColor: Color(0xff4ecca3)),
                    child: Padding(
                      padding: const EdgeInsets.only(top: 35),
                      child: new TextField(
                        style: new TextStyle(fontSize: 18),
                        keyboardType: TextInputType.emailAddress,
                        decoration: InputDecoration(
                            border: new OutlineInputBorder(
                              borderRadius: const BorderRadius.all(
                                const Radius.circular(30.0),
                              ),
                            ),
                            labelText: "First Name",
                            hintText: "ex..John",
                            icon: Icon(Icons.person, size: 23)),
                      ),
                    )),
                Padding(
                  padding: const EdgeInsets.only(top: 35),
                  child: new Theme(
                      data: Theme.of(context)
                          .copyWith(primaryColor: Color(0xff4ecca3)),
                      child: new TextField(
                        style: new TextStyle(fontSize: 18),
                        keyboardType: TextInputType.emailAddress,
                        decoration: InputDecoration(
                            border: new OutlineInputBorder(
                              borderRadius: const BorderRadius.all(
                                const Radius.circular(30.0),
                              ),
                            ),
                            labelText: "Last Name",
                            hintText: "ex..hamd",
                            icon: Icon(Icons.person_outline, size: 23)),
                      )),
                ),
                Padding(
                  padding: const EdgeInsets.only(top: 35),
                  child: new Theme(
                      data: Theme.of(context)
                          .copyWith(primaryColor: Color(0xff4ecca3)),
                      child: new TextField(
                        style: new TextStyle(fontSize: 18),
                        keyboardType: TextInputType.text,
                        decoration: InputDecoration(
                            border: new OutlineInputBorder(
                              borderRadius: const BorderRadius.all(
                                const Radius.circular(30.0),
                              ),
                            ),
                            labelText: "User Name",
                            hintText: "",
                            icon: Icon(Icons.text_fields, size: 23)),
                      )),
                ),
                Padding(
                  padding: const EdgeInsets.only(top: 35),
                  child: new Theme(
                      data: Theme.of(context)
                          .copyWith(primaryColor: Color(0xff4ecca3)),
                      child: new TextField(
                        style: new TextStyle(fontSize: 18),
                        keyboardType: TextInputType.emailAddress,
                        decoration: InputDecoration(
                            border: new OutlineInputBorder(
                              borderRadius: const BorderRadius.all(
                                const Radius.circular(30.0),
                              ),
                            ),
                            labelText: "E-mail",
                            hintText: "example@hotmail.com",
                            icon: Icon(Icons.email, size: 23)),
                      )),
                ),
                Padding(
                  padding: const EdgeInsets.only(top: 35),
                  child: new Theme(
                      data: Theme.of(context)
                          .copyWith(primaryColor: Color(0xff4ecca3)),
                      child: new TextField(
                        style: new TextStyle(fontSize: 18),
                        obscureText: true,
                        keyboardType: TextInputType.emailAddress,
                        decoration: InputDecoration(
                            border: new OutlineInputBorder(
                              borderRadius: const BorderRadius.all(
                                const Radius.circular(30.0),
                              ),
                            ),
                            labelText: "Password",
                            hintText: "enter your new password",
                            icon: Icon(Icons.keyboard, size: 23)),
                      )),
                ),
                Padding(
                  padding: const EdgeInsets.only(top: 35),
                  child: new Theme(
                      data: Theme.of(context)
                          .copyWith(primaryColor: Color(0xff4ecca3)),
                      child: new TextField(
                        style: new TextStyle(fontSize: 18),
                        obscureText: true,
                        keyboardType: TextInputType.emailAddress,
                        decoration: InputDecoration(
                            border: new OutlineInputBorder(
                              borderRadius: const BorderRadius.all(
                                const Radius.circular(30.0),
                              ),
                            ),
                            labelText: "Repeat Password",
                            hintText: "Repeat your new password",
                            icon: Icon(Icons.keyboard, size: 23)),
                      )),
                ),
              ],
            ),
          ),
        ));
  }
}
