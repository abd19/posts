import 'package:flutter/material.dart';
import 'package:posts/UI/LogInPop.dart';
import 'package:posts/UI/SignUpScreen.dart';

class WelcomeScreen extends StatefulWidget {
  @override
  _WelcomeScreenState createState() => _WelcomeScreenState();
}

class _WelcomeScreenState extends State<WelcomeScreen> {
  String pathLogo = "images/Logo.png";

  @override
  Widget build(BuildContext context) {
    return new Stack(
      children: <Widget>[
        new Align(
          child: ClipPath(
            clipper: BackGroundClipper(),
            child: Container(
              decoration: BoxDecoration(
                  gradient: LinearGradient(
                      colors: [ Color(0xFF0086B8),Color(0xFF6EF3FF)],
                      begin: Alignment.topCenter,
                      end: Alignment.bottomCenter)),
            ),
          ),
        ),
        Padding(
          padding: EdgeInsets.only(left: 5,top: 170,right: 5),
          child: new Align(
            alignment: Alignment.topCenter,
            child: new Image.asset(
              pathLogo,
              width: 200.0,
              height: 200.0,
            ),
          ),
        ),
        new Align(
          alignment: Alignment.center,
          child: Padding(
            padding: const EdgeInsets.only(top: 400),
            child: new Row(
              textDirection: TextDirection.ltr,
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Padding(
                  padding: const EdgeInsets.only(right: 10),
                  child: new ButtonTheme(
                    minWidth: 125,
                    child: new RaisedButton(
                      color: Color(0xFF005482),
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(10),
                        side: BorderSide(color: Colors.black, width: 2),
                      ),
                      onPressed: () {
                        Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (BuildContext context) =>
                                    SignUpScreen()));
                      },
                      child: new Text("Sign-Up",
                          textAlign: TextAlign.center,
                          style: new TextStyle(
                              color: Colors.white,
                              fontSize: 25,
                              fontWeight: FontWeight.bold)),
                    ),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(left: 10),
                  child: new ButtonTheme(
                    minWidth: 125,
                    child: new RaisedButton(
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(10),
                          side: BorderSide(color: Colors.black, width: 2)),
                      onPressed: () {
                        showGeneralDialog(
                            barrierColor: Colors.black.withOpacity(0.5),
                            transitionBuilder: (context, a1, a2, widget) {
                              return Transform.scale(
                                scale: a1.value,
                                child: Opacity(
                                  opacity: a1.value,
                                  child: logInPopUp(context),
                                ),
                              );
                            },
                            transitionDuration: Duration(milliseconds: 300),
                            barrierDismissible: false,
                            barrierLabel: '',
                            context: context,
                            pageBuilder: (context, animation1, animation2) {});
                      },
                      child: new Text("Log-In",
                          textAlign: TextAlign.center,
                          style: new TextStyle(
                              color: Colors.white,
                              fontSize: 25,
                              fontWeight: FontWeight.bold)),
                      color: Color(0xFF0086B8),
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      ],
    );
  }
}

class BackGroundClipper extends CustomClipper<Path> {
  @override
  Path getClip(Size size) {
    var path = Path();
    var round = 50.0;
    path.lineTo(0, size.height / 2);

    path.quadraticBezierTo(
        round, size.height * 0.5, size.width - round * 4, size.height * 0.38);
    path.quadraticBezierTo(size.width * 0.9, size.height * 0.2, size.width, 10);
    path.quadraticBezierTo(size.width, 0, size.width * 0.9999, 0);

    return path;
  }

  @override
  bool shouldReclip(CustomClipper<Path> oldClipper) {
    return true;
  }
}
