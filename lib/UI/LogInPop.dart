import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

AlertDialog logInPopUp(BuildContext context) {
  return AlertDialog(
    shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(5),
        side: BorderSide(color: Colors.black, width: 2)),
    title: Center(
      child: new Text(
        "Log-In",
        style: new TextStyle(
          color: Colors.black,
          fontSize: 15,
          fontWeight: FontWeight.bold,
        ),
      ),
    ),
    content: new Container(
      height: 175,
      width: 235,
      child: Padding(
        padding: const EdgeInsets.only(top: 15),
        child: new Column(
          mainAxisAlignment: MainAxisAlignment.start,
          children: <Widget>[
            new Theme(
                data:
                    Theme.of(context).copyWith(primaryColor: Color(0xff4ecca3)),
                child: new TextField(
                  autofocus: true,
                  keyboardType: TextInputType.emailAddress,
                  decoration: InputDecoration(
                      labelText: "E-mail",
                      hintText: "example@hotmail.com",
                      icon: Icon(Icons.email, size: 20)),
                )),
            Padding(
                padding: const EdgeInsets.only(top: 40),
                child: new Theme(
                  data: Theme.of(context)
                      .copyWith(primaryColor: Color(0xff4ecca3)),
                  child: new TextField(
                    keyboardType: TextInputType.text,
                    obscureText: true,
                    decoration: InputDecoration(
                        labelText: "Password",
                        hintText: "Enter Your password",
                        icon: Icon(Icons.vpn_key, size: 20)),
                  ),
                ))
          ],
        ),
      ),
    ),
    actions: <Widget>[
      Padding(
        padding: const EdgeInsets.only(right: 120),
        child: new ButtonTheme(
            minWidth: 10,
            child: new RaisedButton(
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(8)),
              onPressed: () {
                Navigator.of(context).pop();
              },
              child: new Text("Cancel",
                  textAlign: TextAlign.center,
                  style: new TextStyle(
                      color: Colors.black,
                      fontSize: 10,
                      fontWeight: FontWeight.bold)),
              color: Color(0xffeeeeee),
            )),
      ),
      Padding(
        padding: const EdgeInsets.only(
          right: 10,
        ),
        child: new ButtonTheme(
            minWidth: 10,
            child: new RaisedButton(
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(8)),
              onPressed: () {},
              child: new Text("Log-In",
                  textAlign: TextAlign.center,
                  style: new TextStyle(
                      color: Colors.white,
                      fontSize: 10,
                      fontWeight: FontWeight.bold)),
              color: Color(0xff4ecca3),
            )),
      )
    ],
  );
}
